let TYPES = {
    AccountDao: Symbol("AccountDao"),
    AccountService: Symbol("IAccountService"),
    GuestDao: Symbol("GuestDao"),
    GuestService: Symbol("IGuestService"),
    EventDao: Symbol("EventDao"),
    EventService: Symbol("IEventService"),
    DashboardService: Symbol("IDashboardService"),
};

export default TYPES;
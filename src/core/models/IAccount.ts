import { TypeAccess } from "../enum/TypeAccess";
import { BaseModel } from "./IBaseModel";

export interface Account extends BaseModel {
    username: string
    email: string
    type: TypeAccess 
}
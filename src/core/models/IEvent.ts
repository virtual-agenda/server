import { Double } from "typeorm";
import { BaseModel } from "./IBaseModel";
import { Guest } from "./IGuest";

export interface Event extends BaseModel {
    title: string
    description: string
    image: string
    dateStart: Date
    dateEnd: Date
    dateCreation: Date
    locationLat: Number
    locationLng: Number
    guests: Guest[]
}
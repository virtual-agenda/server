import { TypeAccess } from "../enum/TypeAccess";
import { Account } from "./IAccount";
import { BaseModel } from "./IBaseModel";

export interface DashboardInfo extends BaseModel {
    totalEvents: Number
    incomingEvents: Number
    pastEvents: Number,
    participants: Number,
    accountsCreated: Account[],
}
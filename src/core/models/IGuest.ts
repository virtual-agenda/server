import { BaseModel } from "./IBaseModel";
import { Event } from "./IEvent";

export interface Guest extends BaseModel{
    checkIn: boolean
    dateCheckIn: Date
    dateEnd: Date
    accountId: string
    event: Event
    name: string
}
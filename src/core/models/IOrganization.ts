import { BaseModel } from "./IBaseModel";

export interface Organization extends BaseModel {
    name: string
    address: string
    description: string
    phoneNumber: string
}
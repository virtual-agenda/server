import { BaseModel } from "./IBaseModel";

export interface Person extends BaseModel{
    name: string
    lastname: string
    photo: string
    phoneNumber: string
}
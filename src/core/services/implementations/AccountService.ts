import { inject, injectable } from "inversify";
import { AccountDao } from "../../../dao/definitions/IDBAccountDao";
import TYPES from "../../enum/InjectableTypes";
import { Account } from "../../models/IAccount";
import { IAccountService } from "../definitions/IAccountService";

@injectable()
export class AccountService implements IAccountService {
    private _accountDao: AccountDao
    
    constructor(@inject(TYPES.AccountDao) accountDao: AccountDao){
        this._accountDao = accountDao;
    }
    
    async getAccounts(): Promise<Account[]> {
        return this._accountDao.getAccounts();
    }
    createAccount(account: Account): Promise<Account> {
        return this._accountDao.createAccount(account)
    }
    updateAccount(id:string, account: Account): Promise<Account | undefined> {
        return this._accountDao.updateAccount(id, account)
    }
    getAccount(id: string): Promise<Account | undefined> {
        return this._accountDao.getAccount(id)        
    }
    async deleteAccount(id: string): Promise<Account> {
        const account = await this._accountDao.getAccount(id)
        const result = await this._accountDao.deleteAccount(id)
        return result.affected?account:result.raw;
    }

}
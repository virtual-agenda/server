import { inject, injectable } from "inversify";
import { EventDao } from "../../../dao/definitions/IDBEventDao";
import TYPES from "../../enum/InjectableTypes";
import { Event } from "../../models/IEvent";
import { IEventService } from "../definitions/IEventService";

@injectable()
export class EventService implements IEventService {
    private _eventDao: EventDao
    
    constructor(@inject(TYPES.EventDao) eventDao: EventDao){
        this._eventDao = eventDao;
    }
    
    async getEvents(search: string): Promise<Event[]> {
        return this._eventDao.getEvents(search);
    }
    createEvent(event: Event): Promise<Event> {
        event.dateCreation = new Date();
        return this._eventDao.createEvent(event)
    }
    updateEvent(id:string, event: Event): Promise<Event | undefined> {
        return this._eventDao.updateEvent(id, event)
    }
    getEvent(id: string): Promise<Event | undefined> {
        return this._eventDao.getEvent(id)        
    }
    async deleteEvent(id: string): Promise<Event> {
        const event = await this._eventDao.getEvent(id)
        const result = await this._eventDao.deleteEvent(id)
        return result.affected?event:result.raw;
    }

}
import { inject, injectable } from "inversify";
import { EventDao } from "../../../dao/definitions/IDBEventDao";
import { GuestDao } from "../../../dao/definitions/IDBGuestDao";
import TYPES from "../../enum/InjectableTypes";
import { Guest } from "../../models/IGuest";
import { IGuestService } from "../definitions/IGuestService";

@injectable()
export class GuestService implements IGuestService {
    private _guestDao: GuestDao
    private _eventDao: EventDao
    
    constructor(@inject(TYPES.GuestDao) guestDao: GuestDao, @inject(TYPES.EventDao) eventDao: EventDao){
        this._guestDao = guestDao;
        this._eventDao = eventDao;
    }
    
    async getGuests(): Promise<Guest[]> {
        return this._guestDao.getGuests();
    }
    async createGuest(guest: Guest, eventId: string): Promise<Guest> {
        guest.dateCheckIn = new Date();
        let event = await this._eventDao.getEvent(eventId);
        guest.dateEnd = event?.dateEnd??new Date();
        if(event != null){
            guest.event = event;
        }
        return this._guestDao.createGuest(guest)
    }
    updateGuest(id:string, guest: Guest): Promise<Guest | undefined> {
        return this._guestDao.updateGuest(id, guest)
    }
    getGuest(id: string): Promise<Guest | undefined> {
        return this._guestDao.getGuest(id)        
    }
    async deleteGuest(id: string): Promise<Guest> {
        const guest = await this._guestDao.getGuest(id)
        const result = await this._guestDao.deleteGuest(id)
        return result.affected?guest:result.raw;
    }
}
import { inject, injectable } from "inversify";
import { AccountDao } from "../../../dao/definitions/IDBAccountDao";
import { EventDao } from "../../../dao/definitions/IDBEventDao";
import { GuestDao } from "../../../dao/definitions/IDBGuestDao";
import TYPES from "../../enum/InjectableTypes";
import { TypeDateTotal } from "../../enum/TypeDateTotal";
import { DashboardInfo } from "../../models/IDashboardInfo";
import { Event } from "../../models/IEvent";
import { IDashboardService } from "../definitions/IDashboardService";

@injectable()
export class DashboardService implements IDashboardService {
    private _guestDao: GuestDao
    private _eventDao: EventDao
    private _accountDao: AccountDao
    
    constructor(@inject(TYPES.GuestDao) guestDao: GuestDao, @inject(TYPES.EventDao) eventDao: EventDao, @inject(TYPES.AccountDao) accountDao: AccountDao){
        this._guestDao = guestDao;
        this._eventDao = eventDao;
        this._accountDao = accountDao;
    }
    
    async getDashboardInfo(): Promise<DashboardInfo> {
        let dashboard = {} as DashboardInfo;
        var events = await this._eventDao.getEvents("");
        dashboard.totalEvents = this.getDateTotals(events, TypeDateTotal.ALL);
        dashboard.incomingEvents = this.getDateTotals(events, TypeDateTotal.INCOMING);
        dashboard.pastEvents = this.getDateTotals(events, TypeDateTotal.PAST);
        dashboard.participants = (await this._guestDao.getGuests()).filter((obj) => obj.checkIn).length ?? 0;
        dashboard.accountsCreated = await this._accountDao.getAccounts();
        return dashboard;
    }

    private getDateTotals(events: Event[], type: TypeDateTotal): number{
        const today = new Date();
        switch(type){
            case TypeDateTotal.ALL:
                return events.length ?? 0;
            case TypeDateTotal.INCOMING:
                return events.filter((obj) => obj.dateEnd > today).length ?? 0;
            case TypeDateTotal.PAST:
                return events.filter((obj) => obj.dateEnd <= today).length ?? 0;
        }
    }
}
import { Event } from "../../models/IEvent";

export interface IEventService {
    getEvents(search: string): Promise<Event[]>
    createEvent(event: Event): Promise<Event>
    updateEvent(id:string, event: Event): Promise<Event|undefined>
    getEvent(id: string): Promise<Event|undefined>
    deleteEvent(id: string): Promise<Event>
}
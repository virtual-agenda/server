import { DashboardInfo } from "../../models/IDashboardInfo";

export interface IDashboardService {
    getDashboardInfo(): Promise<DashboardInfo>
}
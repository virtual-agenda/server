import { Guest } from "../../models/IGuest";

export interface IGuestService {
    getGuests(): Promise<Guest[]>
    createGuest(guest: Guest, eventId: string): Promise<Guest>
    updateGuest(id:string, guest: Guest): Promise<Guest|undefined>
    getGuest(id: string): Promise<Guest|undefined>
    deleteGuest(id: string): Promise<Guest>
}
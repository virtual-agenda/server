import { Account } from "../../models/IAccount";

export interface IAccountService {
    getAccounts(): Promise<Account[]>
    createAccount(account: Account): Promise<Account>
    updateAccount(id:string, account: Account): Promise<Account|undefined>
    getAccount(id: string): Promise<Account|undefined>
    deleteAccount(id: string): Promise<Account>
}
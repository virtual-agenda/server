import * as express from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, queryParam, request, response } from "inversify-express-utils";
import { JsonResult } from "inversify-express-utils/dts/results";
import TYPES from "../../core/enum/InjectableTypes";
import { Event } from "../../core/models/IEvent";
import { IEventService } from "../../core/services/definitions/IEventService";

@controller("/event")
export class EventController extends BaseHttpController {

    constructor(@inject(TYPES.EventService) private eventService: IEventService) {
        super();
    }

    @httpGet("/")
    public async getEvents(@queryParam("search") search: string, @response() res: express.Response): Promise<JsonResult>{
        const result = await this.eventService.getEvents(search);
        return this.json(result, 200)
    }

    @httpGet("/:id")
    public async getEvent(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.eventService.getEvent(req.params.id);
        return this.json(result, 200)
    }

    @httpPost("/")
    public async createEvent(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const item: Event = req.body;
        console.log(item);
        
        const result = await this.eventService.createEvent(item);
        return this.json(result, 200)
    }

    @httpPut("/:id")
    public async updateEvent(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.eventService.updateEvent(req.params.id, req.body);
        return this.json(result, 200)
    }

    @httpDelete("/:id")
    public async deleteEvent(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.eventService.deleteEvent(req.params.id);
        return this.json(result, 200)
    }
}
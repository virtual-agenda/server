import * as express from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpGet, request, response } from "inversify-express-utils";
import { JsonResult } from "inversify-express-utils/dts/results";
import TYPES from "../../core/enum/InjectableTypes";
import { IDashboardService } from "../../core/services/definitions/IDashboardService";

@controller("/dashboard")
export class DashboardController extends BaseHttpController {

    constructor(@inject(TYPES.DashboardService) private dashboardService: IDashboardService) {
        super();
    }

    @httpGet("/")
    public async getDashboard(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.dashboardService.getDashboardInfo();
        return this.json(result, 200)
    }
}
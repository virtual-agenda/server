import * as express from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, request, response } from "inversify-express-utils";
import { JsonResult } from "inversify-express-utils/dts/results";
import TYPES from "../../core/enum/InjectableTypes";
import { Account } from "../../core/models/IAccount";
import { IAccountService } from "../../core/services/definitions/IAccountService";

@controller("/account")
export class AccountController extends BaseHttpController {

    constructor(@inject(TYPES.AccountService) private accountService: IAccountService) {
        super();
    }

    @httpGet("/")
    public async getAccounts(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.accountService.getAccounts();
        return this.json(result, 200)
    }

    @httpGet("/:id")
    public async getAccount(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.accountService.getAccount(req.params.id);
        return this.json(result, 200)
    }

    @httpPost("/")
    public async createAccount(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const item: Account = req.body;
        console.log(item);
        
        const result = await this.accountService.createAccount(item);
        return this.json(result, 200)
    }

    @httpPut("/:id")
    public async updateAccount(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.accountService.updateAccount(req.params.id, req.body);
        return this.json(result, 200)
    }

    @httpDelete("/:id")
    public async deleteAccount(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.accountService.deleteAccount(req.params.id);
        return this.json(result, 200)
    }
}
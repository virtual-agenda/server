import * as express from "express";
import { inject } from "inversify";
import { BaseHttpController, controller, httpDelete, httpGet, httpPost, httpPut, queryParam, request, response } from "inversify-express-utils";
import { JsonResult } from "inversify-express-utils/dts/results";
import TYPES from "../../core/enum/InjectableTypes";
import { Guest } from "../../core/models/IGuest";
import { IGuestService } from "../../core/services/definitions/IGuestService";

@controller("/guest")
export class GuestController extends BaseHttpController {

    constructor(@inject(TYPES.GuestService) private guestService: IGuestService) {
        super();
    }

    @httpGet("/")
    public async getGuests(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.guestService.getGuests();
        return this.json(result, 200)
    }

    @httpGet("/:id")
    public async getGuest(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.guestService.getGuest(req.params.id);
        return this.json(result, 200)
    }

    @httpPost("/")
    public async createGuest(@request() req: express.Request, @queryParam("eventId") eventId: string, @response() res: express.Response): Promise<JsonResult>{
        const item: Guest = req.body;
        const result = await this.guestService.createGuest(item, eventId);
        return this.json(result, 200)
    }

    @httpPut("/:id")
    public async updateGuest(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.guestService.updateGuest(req.params.id, req.body);
        return this.json(result, 200)
    }

    @httpDelete("/:id")
    public async deleteGuest(@request() req: express.Request,@response() res: express.Response): Promise<JsonResult>{
        const result = await this.guestService.deleteGuest(req.params.id);
        return this.json(result, 200)
    }
}
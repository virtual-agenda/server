/**
 * Required External Modules
 */
import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import "reflect-metadata";
import {createConnection} from "typeorm";
import { InversifyExpressServer } from "inversify-express-utils";
import container from "../inversify.config";
import "./api/controllers/accountController"
import "./api/controllers/guestController"
import "./api/controllers/eventController"
import "./api/controllers/dashboardController"

dotenv.config();

/**
 * App Variables
 */
if (!process.env.PORT) {
    process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);

const app = express();

const connection = createConnection();
/**
 *  App Configuration
 */
app.use(cors());
app.use(express.json());
app.use(express.urlencoded());
let server =  new InversifyExpressServer(container, null, { rootPath: "/api" }, app);
let appConfigured = server.build();
/**
 * Server Activation
 */
 appConfigured.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});
/**
 * DB connection
 */
connection.then(connection => {
    console.log("connection to DB successfull");
}).catch(error => console.log(error));
import { Account } from "../../core/models/IAccount";
import { AccountDao } from "../definitions/IDBAccountDao";
import { createConnection, DeleteResult, getRepository } from "typeorm";
import { DBAccount } from "../entities/DBAccount";
import { injectable } from "inversify";

@injectable()
export class DBAccountDao implements AccountDao{
    
    async getAccounts(): Promise<Account[]> {
        const result = await getRepository(DBAccount).find();
        return result
    }
    async createAccount(account: Account): Promise<Account> {
        const result = await getRepository(DBAccount).save(account);
        return result;
    }
    async updateAccount(id:string, account: Account): Promise<Account | undefined> {
        await getRepository(DBAccount).update(id, account);
        const result = await getRepository(DBAccount).findOne(id);
        return result;
    }
    async getAccount(id: string): Promise<Account | undefined> {
        const result = await getRepository(DBAccount).findOne(id);
        return result
    }
    async deleteAccount(id: string): Promise<DeleteResult> {
        const result = await getRepository(DBAccount).delete(id);
        return result
    }

}
import { Guest } from "../../core/models/IGuest";
import { GuestDao } from "../definitions/IDBGuestDao";
import { createConnection, DeleteResult, getRepository } from "typeorm";
import { DBGuest } from "../entities/DBGuest";
import { injectable } from "inversify";
import { DBEvent } from "../entities/DBEvent";

@injectable()
export class DBGuestDao implements GuestDao{
    
    async getGuests(): Promise<Guest[]> {
        const result = await getRepository(DBGuest).find({ relations: ["event"] });
        return result
    }
    async createGuest(guest: Guest): Promise<Guest> {
        const result = await getRepository(DBGuest).save(guest);
        return result;
    }
    async updateGuest(id:string, guest: Guest): Promise<Guest | undefined> {
        await getRepository(DBGuest).update(id, guest);
        const result = await getRepository(DBGuest).findOne(id);
        return result;
    }
    async getGuest(id: string): Promise<Guest | undefined> {
        const result = await getRepository(DBGuest).findOne(id);
        return result
    }
    async deleteGuest(id: string): Promise<DeleteResult> {
        const result = await getRepository(DBGuest).delete(id);
        return result
    }

}
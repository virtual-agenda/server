import { Event } from "../../core/models/IEvent";
import { EventDao } from "../definitions/IDBEventDao";
import { createConnection, DeleteResult, getRepository, IsNull, Like } from "typeorm";
import { DBEvent } from "../entities/DBEvent";
import { injectable } from "inversify";
import { title } from "process";

@injectable()
export class DBEventDao implements EventDao{
    
    async getEvents(search: string): Promise<Event[]> {
        const result = await getRepository(DBEvent)
                             .createQueryBuilder();
        // Search queries for any word that matches the criteria.
        if(search){
            result.where([
                { title : Like(`%${search}%`)},
                { description : Like(`%${search}%`)}
            ])
        }
                
        let events = await result.getMany();
        return events;
    }

    async createEvent(event: Event): Promise<Event> {
        const result = await getRepository(DBEvent).save(event);
        return result;
    }
    async updateEvent(id:string, event: Event): Promise<Event | undefined> {
        await getRepository(DBEvent).update(id, event);
        const result = await getRepository(DBEvent).findOne(id);
        return result;
    }
    async getEvent(id: string): Promise<Event | undefined> {
        const result = await getRepository(DBEvent).findOne(id);
        return result
    }
    async deleteEvent(id: string): Promise<DeleteResult> {
        const result = await getRepository(DBEvent).delete(id);
        return result
    }

}
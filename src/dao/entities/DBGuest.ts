import { Column, Entity, Generated, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Guest } from "../../core/models/IGuest";
import { Event } from "../../core/models/IEvent";
import { DBEvent } from "./DBEvent";

@Entity({name: "guest"})
export class DBGuest implements Guest {
    @Column()
    checkIn: boolean = false;
    @Column({name: "date_check_in"})
    dateCheckIn!: Date;
    @Column({name: "date_end"})
    dateEnd!: Date;
    @Column({name: "account_id"})
    accountId!: string;
    @Column({name: "name"})
    name!: string;
    @PrimaryGeneratedColumn()
    id!: string;
    @ManyToOne(() => DBEvent, event => event.guests)
    @JoinTable()
    event!: Event;
    @Column()
    @Generated("uuid")
    uuid!: string;
}
import { Column, Entity, Generated, PrimaryGeneratedColumn } from "typeorm";
import { TypeAccess } from "../../core/enum/TypeAccess";
import { Account } from "../../core/models/IAccount";

@Entity({name: "account"})
export class DBAccount implements Account {
    @Column()
    username!: string;
    @Column()
    email!: string;
    @Column()
    type!: TypeAccess;
    @PrimaryGeneratedColumn()
    id!: string;
    @Column()
    @Generated("uuid")
    uuid!: string;
}
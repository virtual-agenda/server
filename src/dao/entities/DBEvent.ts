import { Column, Double, Entity, Generated, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Event } from "../../core/models/IEvent";
import { Guest } from "../../core/models/IGuest";
import { DBGuest } from "./DBGuest";

@Entity({name: "event"})
export class DBEvent implements Event {
    @Column()
    dateEnd!: Date;
    @Column()
    title!: string;
    @Column()
    description!: string;
    @Column()
    image!: string;
    @Column({name: "date_start"})
    dateStart!: Date;
    @Column({name: "date_creation"})
    dateCreation!: Date;
    @Column({name: "location_lat", type: "double"})
    locationLat!: Number;
    @Column({name: "location_lng", type: "double"})
    locationLng!: Number;
    @OneToMany(() => DBGuest, guest => guest.event)
    @JoinTable()
    guests!: Guest[];
    @PrimaryGeneratedColumn()
    id!: string;
    @Column()
    @Generated("uuid")
    uuid!: string;
}
import { DeleteResult } from "typeorm";
import { Guest } from "../../core/models/IGuest";

export interface GuestDao {
    getGuests(): Promise<Guest[]>
    createGuest(guest: Guest): Promise<Guest>
    updateGuest(id:string, guest: Guest): Promise<Guest|undefined>
    getGuest(id: string): Promise<Guest|undefined>
    deleteGuest(id: string): Promise<DeleteResult>
}
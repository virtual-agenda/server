import { DeleteResult } from "typeorm";
import { Account } from "../../core/models/IAccount";

export interface AccountDao {
    getAccounts(): Promise<Account[]>
    createAccount(account: Account): Promise<Account>
    updateAccount(id:string, account: Account): Promise<Account|undefined>
    getAccount(id: string): Promise<Account|undefined>
    deleteAccount(id: string): Promise<DeleteResult>
}
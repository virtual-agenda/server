import { DeleteResult } from "typeorm";
import { Event } from "../../core/models/IEvent";

export interface EventDao {
    getEvents(search: string): Promise<Event[]>
    createEvent(event: Event): Promise<Event>
    updateEvent(id:string, event: Event): Promise<Event|undefined>
    getEvent(id: string): Promise<Event|undefined>
    deleteEvent(id: string): Promise<DeleteResult>
}
import { Container } from "inversify";
import TYPES from "./src/core/enum/InjectableTypes";
import { IAccountService } from "./src/core/services/definitions/IAccountService";
import { IDashboardService } from "./src/core/services/definitions/IDashboardService";
import { IEventService } from "./src/core/services/definitions/IEventService";
import { IGuestService } from "./src/core/services/definitions/IGuestService";
import { AccountService } from "./src/core/services/implementations/AccountService";
import { DashboardService } from "./src/core/services/implementations/DashboardService";
import { EventService } from "./src/core/services/implementations/EventService";
import { GuestService } from "./src/core/services/implementations/GuestService";
import { AccountDao } from "./src/dao/definitions/IDBAccountDao";
import { EventDao } from "./src/dao/definitions/IDBEventDao";
import { GuestDao } from "./src/dao/definitions/IDBGuestDao";
import { DBAccountDao } from "./src/dao/implementations/DBAccountDao";
import { DBEventDao } from "./src/dao/implementations/DBEventDao";
import { DBGuestDao } from "./src/dao/implementations/DBGuestDao";

var container = new Container();
container.bind<IAccountService>(TYPES.AccountService).to(AccountService);
container.bind<AccountDao>(TYPES.AccountDao).to(DBAccountDao);
container.bind<IGuestService>(TYPES.GuestService).to(GuestService);
container.bind<GuestDao>(TYPES.GuestDao).to(DBGuestDao);
container.bind<IEventService>(TYPES.EventService).to(EventService);
container.bind<EventDao>(TYPES.EventDao).to(DBEventDao);
container.bind<IDashboardService>(TYPES.DashboardService).to(DashboardService);

export default container;